<?php

/**
 * Employees finder
 *
 * Author: Student Name
 * Company: BrainAcademy
 * Date: some date
 */

function getEmployeeInfo($employye) {
    $resultString = "Name : {$employye['name']}\n";
    $resultString .= "Age : {$employye['age']}\n";
    $resultString .= "Skills : " . implode(', ', $employye['skills']);
    return $resultString;
}

$employees = [
    [
        'name' => 'Clark Kent',
        'age' => 22,
        'skills' => ['PHP', 'Java', 'C#']
    ],
    [
        'name' => 'Steve Stifler',
        'age' => 21,
        'skills' => ['PHP', 'JS', 'CSS', 'HTML']
    ],
    [
        'name' => 'Bruce Wayne',
        'age' => 35,
        'skills' => ['PHP', 'PHP Unit', 'XDebug', 'JS']
    ],
    [
        'name' => 'Peter Parker',
        'age' => 18,
        'skills' => ['PHP', 'C', 'Pascal']
    ]
];

$skill = isset($argv[1]) ? $argv[1] : 0;

foreach ($employees as $employee) {
    if ( !( in_array($skill, $employee['skills']) ) ) continue;

    echo PHP_EOL . getEmployeeInfo($employee) . PHP_EOL;
}
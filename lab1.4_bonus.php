<?php

//$currentMonth   = date('m');
//$currentWeekDay = date('N');

/*  Student should create function getMonthName witch will accept parameter with month number
 *  and return name of needed month */
function getMonthName($n) {
    switch ($n) {
        case 1:
            return 'January';
        case 2:
            return 'February';
        case 3:
            return 'March';
        case 4:
            return 'April';
        case 5:
            return 'May';
        case 6:
            return 'June';
        case 7:
            return 'July';
        case 8:
            return 'August';
        case 9:
            return 'September';
        case 10:
            return 'October';
        case 11:
            return 'November';
        case 12:
            return 'December';

        default:
            echo "Error: Unknow month!";
            return false;
    }
}

/*  Student should create function getWeekDays witch will return array with relation of numbers and names of days   */
function getWeekDays() {

    $lastSunday = strtotime('last Sunday');
    $weekDays = [];

    for ($i = 1; $i <= 7; $i++) {
        $day = $i * (24 * 60 * 60);
        $dayCounter = $lastSunday + $day;
        $weekDays[$i] = date("l", $dayCounter);
    }

    return $weekDays;
}

/*  Student should create function getMonthCalendar witch will accept parameters with number of month and year
 *  and return array of arrays of weeks with days in the current month   */
function getMonthCalendar($month, $year) {
    $overallDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
    $monthCalendar = [];
    $weekNumber = 1;
    $dayOfMonth = 1;
    while($dayOfMonth <= $overallDays) {
        for($i = 1; $i <= 7; $i++) {
            $dayOfWeek = date('N', strtotime("{$dayOfMonth}.{$month}.{$year}"));
            if($i == $dayOfWeek && $dayOfMonth <= $overallDays) {
                $monthCalendar[$weekNumber][$i] = $dayOfMonth;
                $dayOfMonth++;
            } else {
                $monthCalendar[$weekNumber][$i] = null;
            }
        }
        $weekNumber++;
    }
    return $monthCalendar;
}

/*  Student should create function buildCalendarTable witch will call function getMonthCalendar
 *  and convert result to user-friendly table */
function buildCalendarTable() {
    $randMonth = mt_rand(1, 12);
    $randYear = mt_rand(1970, 2037);
    $monthCalendar = getMonthCalendar($randMonth, $randYear);
    $weekDays = getWeekDays();

    //=========================== Building the table
    echo '<table align="center" border="1" cellspacing="0" cellpadding="8">';
    echo '<caption><h3>' . getMonthName($randMonth) . ', ' . $randYear . '</h3></caption>';
    echo '<tr>';
    for($i = 1; $i <= 7; $i++) {
        echo '<th>' . $weekDays[$i] . '</th>';
    }
    echo '</tr>';
    foreach($monthCalendar as $weeks) {
        echo '<tr>';
        foreach($weeks as $day) {
            echo '<td>' . $day . '</td>';
        }
        echo '</tr>';
    }
    echo '</table>';

}

buildCalendarTable();
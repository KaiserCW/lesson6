<?php

// * Student should to provide an interface for receiving the minimum and maximum range values
$minNum = isset($argv[1]) ? $argv[1] : 0;
$maxNum = isset($argv[2]) ? $argv[2] : 0;

$evenNumbers = [];

if ($minNum > $maxNum) {
    die('Error: You have to type first number less than second!');
}

// * Student should to find all even numbers from specified interval
for($i = $minNum; $i <= $maxNum; $i++) {
    if ($i % 2) continue;
    $evenNumbers[] = $i;
}

// * Student should to print founded numbers separated by commas
echo implode(', ', $evenNumbers);
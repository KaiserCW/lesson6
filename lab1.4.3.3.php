<?php

$hashes = [
    'c9e1074f5b3f9fc8ea15d152add07294',
    'a97da629b098b75c294dffdc3e463904',
    '2723d092b63885e0d7c260cc007e8b9d',
    '38b3eff8baf56627478ec76a704e9b52'
];

$newHashes = [];

$i = 100;

do {
    $currentHash = md5($i);
    if (in_array($currentHash, $hashes)){
        echo $currentHash . PHP_EOL;
    } else {
        $newHashes[] = $currentHash;
    }
    $i++;
} while ($i <= 110);

echo 'New array: ' . PHP_EOL . implode(PHP_EOL, $newHashes);

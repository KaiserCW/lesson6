<?php

  /* Student should create script which will generate random boolean value and check it:
   * While value is TRUE stone is jumping
   * If value is FALSE stone id drowned */

  $counter = 0;

  do {
      $counter++;
      $isJumping = mt_rand(0, 1);
      echo 'Jump! -> ';
      sleep(1);
  } while($isJumping && $counter < 15);